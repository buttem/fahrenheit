package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTestTWO {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testConvertFromCelsius() {
		int resultValue = Fahrenheit.convertFromCelsius(0);
		assertTrue("Valid input" , (resultValue == 32));
	}
	
	@Test
	public void testConvertFromCelsiusException() {
		int resultValueTwo = Fahrenheit.convertFromCelsius(-5);
		assertTrue("invalid input" , (resultValueTwo == 23));	
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		int resultValueThree = Fahrenheit.convertFromCelsius(5);
		assertTrue("Almost valid input" , (resultValueThree == 41));
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		int resultValueFour = Fahrenheit.convertFromCelsius(100);
		assertTrue("Almost valid input" , (resultValueFour == 212));
	}
	
	

}
