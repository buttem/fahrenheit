package sheridan;

public class Fahrenheit {

    public static void main(String[] args) {
    	
    	int input = 1;
     	System.out.println(convertFromCelsius(input));
     	
     	
     	int inputTestOne = 23;
    	double resultOne = Double.valueOf(convertFromCelsius(inputTestOne));
    	System.out.println("Value in Celsius = "+ inputTestOne + "�C" + " --- Value in Fahrenheit = " + String.format("%1.2f", resultOne) + "�F");
    	
    	System.out.println("--------------------------------");
    	
    	int inputTestThree = 100;
    	double resultTwo = Double.valueOf(convertFromCelsius(inputTestThree));
    	System.out.println("Value in Celsius = "+ inputTestThree + "�C" + " --- Value in Fahrenheit = " + String.format("%1.2f", resultTwo) + "�F");
    
	}
	
	
	public static int convertFromCelsius(int i) {
		return i * 9/5 + 32 ;
	}
}
